# COVID19 Plot Argentina #
Este script de Python usa la librería Matplotlib para plotear la curva de casos publicada diariamente en la [cuenta de Tweeter de J. Fraire](https://twitter.com/TotinFraire).

## Fuentes de Datos ##
Toma automáticamente los datos publicados por [Infomapuche](https://twitter.com/infomapache) en el repositorio [Covid19arData](https://github.com/SistemasMapache/Covid19arData) y [Jorge Aliaga](https://twitter.com/jorgeluisaliaga) en su [registro público](https://docs.google.com/spreadsheets/d/1M7uDgWSfy6z1MNbC9FP6jTNgvY7XchJ0m-BfW88SKtQ/edit?usp=sharing).

## Información en el Gráfico ##
Para cada provincia se muestran valores de casos y fallecidos acumulados. Además, en la curva se puede apreciar los fallecimientos de cada provincia en el tiempo. Los recuperados se indican en línea punteada y el área sombreada por encima indica infectados activos.


## Como correr

#### Instalar dependencias
```bash
python3 -m pip install matplotlib imageio
```

#### Primera ejecucion
```bash
python3 cv-ar.py --update
```

Para la primera ejecucion usar `--update` para bajar los datos iniciales, asi como tambien para actualizar los datos desde las fuentes cada vez que se requiera.

