# -*- coding: utf-8 -*-

import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.transforms as transforms
from matplotlib.transforms import offset_copy
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from matplotlib.patches import FancyBboxPatch
from matplotlib.ticker import StrMethodFormatter
from matplotlib.ticker import NullFormatter
from matplotlib.ticker import FuncFormatter
import datetime
from datetime import timedelta
import urllib.request
import csv
import math
import numpy as np
import copy
import imageio
import argparse


# Input parameters
parser = argparse.ArgumentParser(description='COVID-19 Plot Argentina.')
parser.add_argument('--update', action='store_true', help='Actualiza el archivo de datos')
parser.add_argument('--absolute', dest='add_new_values_absolute', action='store_true', default=True, help='Agregar valores absolutos')
parser.add_argument('--relative', dest='add_new_values_relative', action='store_true', help='Agregar valores relativos')
args = parser.parse_args()

date_time_now = datetime.datetime.now()
date_time_now = datetime.datetime(
    date_time_now.year, date_time_now.month, date_time_now.day)
date_time_start = date_time_now - timedelta(days=20)

# data from 11/4
new_values_absolute = [
    ("Argentina", 2142, 89), 
    ("Bs. As.", 544, 35),
    ("CABA", 578, 26),
    ("Catamarca", 0, 0),
    ("Chaco", 149, 8),
    ("Chubut", 0, 0),
    ("Córdoba", 201, 2),
    ("Corrientes", 24, 0),
    ("Entre Ríos", 22, 0),
    ("Formosa", 0, 0),
    ("Jujuy", 5, 0),
    ("La Pampa", 5, 0),
    ("La Rioja", 18, 1),
    ("Mendoza", 54, 4),
    ("Misiones", 3, 0),
    ("Neuquén", 85, 5),
    ("Río Negro", 65, 2),
    ("Salta", 3, 0),
    ("San Juan", 2, 0),
    ("San Luis", 11, 0),
    ("Sta. Cruz", 40, 0),
    ("Sta. Fe", 200, 2),
    ("Stgo. del E.", 12, 0),
    ("T. del Fgo.", 91, 0),
    ("Tucumán", 30, 4)
]

new_values_relative = [
    ("not used", 0, 0),
    ("Buenos Aires", 0, 0),
    ("CABA", 0, 0),
    ("Catamarca", 0, 0),
    ("Chaco", 0, 0),
    ("Chubut", 0, 0),
    ("Córdoba", 0, 0),
    ("Corrientes", 0, 0),
    ("Entre Ríos", 0, 0),
    ("Formosa", 0, 0),
    ("Jujuy", 0, 0),
    ("La Pampa", 0, 0),
    ("La Rioja", 0, 0),
    ("Mendoza", 0, 0),
    ("Misiones", 0, 0),
    ("Neuquén", 0, 0),
    ("Río Negro", 0, 0),
    ("Salta", 0, 0),
    ("San Juan", 0, 0),
    ("San Luis", 0, 0),
    ("Santa Cruz", 0, 0),
    ("Santa Fe", 0, 0),
    ("Stgo. del Estero", 0, 0),
    ("T. del Fuego", 0, 0),
    ("Tucumán", 0, 0)
]

# provinces and population
countries = [
    ("Argentina", 40117000),
    ("Bs. As.", 15625084),
    ("CABA", 2890151),
    ("Catamarca", 367828),
    ("Chaco", 1055259),
    ("Chubut", 509108),
    ("Córdoba", 3308876),
    ("Corrientes", 992595),
    ("Entre Ríos", 1235994),
    ("Formosa", 530162),
    ("Jujuy", 673307),
    ("La Pampa", 318951),
    ("La Rioja", 333642),
    ("Mendoza", 1738929),
    ("Misiones", 1101593),
    ("Neuquén", 551266),
    ("Río Negro", 638645),
    ("Salta", 1214441),
    ("San Juan", 681055),
    ("San Luis", 432310),
    ("Sta. Cruz", 273964),
    ("Sta. Fe", 3194537),
    ("Stgo. del E.", 874006),
    ("T. del Fgo.", 127205),
    ("Tucumán", 1448188)
]

###############
# Code start
###############

def K_formatter(number, pos=None):
    magnitude = 0
    while abs(number) >= 1000:
        magnitude += 1
        number /= 1000.0
    return '%d%s' % (number, ['', 'K', 'M', 'B', 'T', 'Q'][magnitude])

dates = {}
for country in countries:
    dates[country] = []

total_cases = {}
for country in countries:
    total_cases[country] = []

total_deaths = {}
for country in countries:
    total_deaths[country] = []

death_ratio = {}
for country in countries:
    death_ratio[country] = []

total_recovered = {}
for country in countries:
    total_recovered[country] = []

total_tests = {}
for country in countries:
    total_tests[country] = []


# Download latest @infomapuche data and save it locally:
# https://docs.google.com/spreadsheets/d/16-bnsDdmmgtSxdWbVMboIHo5FRuz76DBxsz_BbsEVWA/edit#gid=0
# https://github.com/SistemasMapache/Covid19arData
url3 = "https://raw.githubusercontent.com/SistemasMapache/Covid19arData/master/CSV/Covid19arData%20-%20historico.csv"
file_name3 = "data_arg.csv"
if args.update:
    urllib.request.urlretrieve(url3, file_name3)

# parse argentina data
try:
    f = open(file_name3, 'r')
except:
    print('Error abriendo ' + file_name3 + ". Si es primera ejecucion, correr con --update")
    raise
else:
    with f as file:
        csv_reader = csv.reader(file, delimiter=',')
        for row in csv_reader:
            if row[0] == "fecha" or row[0] == "":
                continue

            r_date = row[0]
            r_day = row[1]
            r_day_quar = row[2]
            r_argentina = row[3]
            r_country = row[4]
            if r_country == "CÃ³rdoba":
                r_country = "Córdoba"
            if r_country == "NeuquÃ©n":
                r_country = "Neuquén"
            if r_country == "RÃ­o Negro":
                r_country = "Río Negro"
            if r_country == "TucumÃ¡n":
                r_country = "Tucumán"
            if r_country == "Entre RÃ­os":
                r_country = "Entre Ríos"
            if r_country == "Ciudad de Buenos Aires":
                r_country = "CABA"
            if r_country == "Buenos Aires":
                r_country = "Bs. As."
            if r_country == "Santa Fe":
                r_country = "Sta. Fe"
            if r_country == "Santa Cruz":
                r_country = "Sta. Cruz"
            if r_country == "Tierra Del Fuego":
                r_country = "T. del Fgo."
            if r_country == "Tierra del Fuego":
                r_country = "T. del Fgo."
            if r_country == "Santiago del Estero":
                r_country = "Stgo. del E."
            if r_country == "Santiago Del Estero":
                r_country = "Stgo. del E."
            r_reg = row[5]
            r_total_cases = row[6]
            r_new_cases = row[7]
            r_total_deaths = row[8]
            if r_total_deaths == '':
                r_total_deaths = "0"
            r_new_deaths = row[9]
            if r_new_deaths == '':
                r_new_deaths = "0"
            r_total_recovered = row[10]
            if r_total_recovered == '':
                r_total_recovered = "0"
            r_tx_type = row[11]
            r_inform = row[12]

            date_time_obj = datetime.datetime.strptime(r_date, '%d/%m/%Y')

            # Add to provinces
            country_found = False
            for country in countries:
                if r_country == country[0]:
                    country_found = True
                    if date_time_obj in dates[country]:
                        total_cases[country][-1] += int(r_new_cases)
                        total_deaths[country][-1] += int(r_new_deaths)
                        death_ratio[country][-1] = total_deaths[country][-1] / total_cases[country][-1]
                    else:
                        dates[country].append(date_time_obj)
                        if total_cases[country]:
                            total_cases[country].append(
                                total_cases[country][-1] + int(r_new_cases))
                            total_deaths[country].append(
                                total_deaths[country][-1] + int(r_new_deaths))
                        else:
                            total_cases[country].append(int(r_new_cases))
                            total_deaths[country].append(int(r_new_deaths))
                        death_ratio[country].append(total_deaths[country][-1] / total_cases[country][-1])
            if not country_found and (r_new_cases != "0" or r_new_deaths != "0") :
                print("country not found:", r_country, r_new_cases, r_new_deaths)

            # Add to argentina
            if date_time_obj in dates[countries[0]]:
                total_cases[countries[0]][-1] += int(r_new_cases)
                total_deaths[countries[0]][-1] += int(r_new_deaths)
                death_ratio[countries[0]][-1] = total_deaths[countries[0]][-1] / total_cases[countries[0]][-1]
            else:
                dates[countries[0]].append(date_time_obj)
                if total_cases[countries[0]]:
                    total_cases[countries[0]].append(total_cases[countries[0]][-1] + int(r_new_cases))
                    total_deaths[countries[0]].append(total_deaths[countries[0]][-1] +int(r_new_deaths))
                else:
                    total_cases[countries[0]].append(int(r_new_cases))
                    total_deaths[countries[0]].append(int(r_new_deaths))
                death_ratio[countries[0]].append(total_deaths[countries[0]][-1] / total_cases[countries[0]][-1])

# Download tests sheet @jorgealiaga
# https://docs.google.com/spreadsheets/d/1M7uDgWSfy6z1MNbC9FP6jTNgvY7XchJ0m-BfW88SKtQ/edit?usp=sharing
url4 = "https://docs.google.com/spreadsheets/d/e/2PACX-1vS5-67uMxlfDLSmYvxMcZDiSffCG2MeL75gSUvuDvPLgbpHwC3TAOwBPeBYoiRi-rT2Qkt9u5aCCQrL/pub?gid=0&single=true&output=csv"
file_name4 = "data_arg_tests.csv"
if args.update:
    urllib.request.urlretrieve(url4, file_name4)

# init tests and recovered in argetina
for date in dates[countries[0]]:
    total_tests[countries[0]].append(0)
    total_recovered[countries[0]].append(0)

# parse test data
try:
    f = open(file_name4, 'r')
except:
    print('Error abriendo ' + file_name4 + ". Si es primera ejecucion, correr con --update")
    raise
else:
    with open(file_name4, 'r') as file:
        csv_reader = csv.reader(file, delimiter=',')
        for row in csv_reader:
            if row[0] == "":
                continue
            if row[0][0] == "F":
                continue

            r_date = row[0]
            r_recuperados = row[3]
            if r_recuperados == "":
                r_recuperados = "0"
            r_tests_negativos = row[5]
            if r_tests_negativos == "":
                r_tests_negativos = "0"
            r_tests_totales = row[6]
            if r_tests_totales == "":
                r_tests_totales = "0"

            date_time_obj = datetime.datetime.strptime(r_date, '%d/%m/%Y')

            if date_time_obj in dates[countries[0]]:
                index = dates[countries[0]].index(date_time_obj)
                total_tests[countries[0]][index] += int(r_tests_totales)
                total_recovered[countries[0]][index] = int(r_recuperados)

# ensure days with no test registered get value from previous day
for t, tests in enumerate(total_tests[countries[0]]):
    if total_tests[countries[0]][t] == 0 and t != 0:
        total_tests[countries[0]][t] = total_tests[countries[0]][t - 1]

# ensure data is accumulated to last date
if total_recovered[countries[0]][-1] == 0:
    total_recovered[countries[0]][-1] = total_recovered[countries[0]][-2]
if total_tests[countries[0]][-1] == 0:
    total_tests[countries[0]][-1] = total_tests[countries[0]][-2]

###############
# Filter dates
###############
orig_dates = copy.deepcopy(dates)
date_time_latest = datetime.datetime.strptime("2019-01-01", '%Y-%m-%d')
for country in countries:
    for i in range(len(orig_dates[country])):
        if orig_dates[country][i] > date_time_latest:
            date_time_latest = orig_dates[country][i]
for country in countries:
    if dates[country]:
        if dates[country][-1] != date_time_latest:
            dates[country].append(date_time_latest)
            total_cases[country].append(total_cases[country][-1])
            total_deaths[country].append(total_deaths[country][-1])
            if country[0] == "Argentina":
                total_deaths[country].append(total_deaths[country][-1])
                death_ratio[country].append(death_ratio[country][-1])

##########################
# Add new values relative
##########################
if args.add_new_values_relative:
    for country in countries:
        for value in new_values_relative:
            if value[0] == country[0]:

                # Add to provinces
                dates[country].append(date_time_latest + timedelta(days=1))
                if total_cases[country]:
                    total_cases[country].append(total_cases[country][-1] + value[1])
                    total_deaths[country].append(
                    total_deaths[country][-1] + value[2])
                else:
                    total_cases[country].append(value[1])
                    total_deaths[country].append(value[2])
                if total_cases[country][-1] != 0:
                    death_ratio[country].append(total_deaths[country][-1] / total_cases[country][-1])
                else:
                    death_ratio[country].append(0)
                total_recovered[country].append(total_recovered[country][-1])

                # Add to argentina
                if date_time_latest + timedelta(days=1) in dates[countries[0]]:
                    total_cases[countries[0]][-1] = total_cases[countries[0]][-1] + value[1]
                    total_deaths[countries[0]][-1] = total_deaths[countries[0]][-1] + value[2]
                    death_ratio[countries[0]][-1] = total_deaths[countries[0]][-1] / total_cases[countries[0]][-1]
                else:
                    dates[countries[0]].append(date_time_latest + timedelta(days=1))
                    total_cases[countries[0]].append(total_cases[countries[0]][-1] + value[1])
                    total_deaths[countries[0]].append(total_deaths[countries[0]][-1] + value[2])
                    death_ratio[countries[0]].append(total_deaths[countries[0]][-1] / total_cases[countries[0]][-1])
                    total_recovered[countries[0]].append(total_recovered[countries[0]][-1])

    date_time_latest = date_time_latest + timedelta(days=1)

##########################
# Add new values absolute
##########################
if args.add_new_values_absolute:
    for country in countries:
        for value in new_values_absolute:
            if value[0] == country[0]:

                # Add to provinces
                dates[country].append(date_time_latest + timedelta(days=1))
                total_cases[country].append(value[1])
                total_deaths[country].append(value[2])
                if total_cases[country][-1] != 0:
                    death_ratio[country].append(total_deaths[country][-1] / total_cases[country][-1])
                else:
                    death_ratio[country].append(0)
                if total_recovered[country]:
                    total_recovered[country].append(total_recovered[country][-1])
                else:
                    total_recovered[country].append(0)

    date_time_latest = date_time_latest + timedelta(days=1)

# add item 0 to provinces with 0 cases
for country in countries:
    if not total_cases[country]:
        dates[country].append(date_time_latest)
        total_cases[country].append(0)
        total_deaths[country].append(0)


all_dates = [date_time_start + datetime.timedelta(days=x)
             for x in range(0, (date_time_latest-date_time_start).days + 2)]

###############
# sort countries
###############

countries_by_cases = sorted(countries, key=lambda x: total_cases[x][-1])

###############
# plot cases
###############
# Set font
font = {'family': 'serif',
        'weight': 'normal',
        'size': 8}
plt.rc('font', **font)

fig, ax = plt.subplots(figsize=(8, 8))
ax.set_axisbelow(True)
plt.grid(axis='x', which='both', color='0.95')
plt.grid(axis='y', which='major', color='0.93')
plt.grid(axis='y', which='minor', color='0.96')

# shrink current axis by 10%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])

# blended transform
trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)

# y axis
plt.yscale("log")
plt.ylim(bottom=1, top=3500)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:.0f}'))
ax.yaxis.set_major_formatter(FuncFormatter(K_formatter))
ax.yaxis.set_minor_formatter(NullFormatter())
ax.set_ylabel("Casos confirmados de COVID-19 acumulados (escala logarítmica) (1)")
# x axis
plt.xlim(left=mdates.date2num(all_dates[0]))
plt.xlim(right=mdates.date2num(all_dates[-1]))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m'))
plt.xticks(all_dates, rotation=45, ha='right', rotation_mode="anchor")
xticks = ax.xaxis.get_major_ticks()
xticks[-1].label1.set_visible(False)

argentina_alpha = 1
argentina_marker = '-o'
argentina_linewidth = 3
argentina_markersize = 8
argentina_markeredgecolor = 'royalblue'

country_alpha = 1
country_marker = '-o'
country_linewidth = 2
country_markersize = 6

# others: jet, veridis, magma
color_map = plt.cm.coolwarm(np.linspace(0,1,len(countries_by_cases)))

color_death = color_map[20]
color_rate = color_map[13]
color_back = color_map[8]
test_color = "royalblue"

for i, country in enumerate(countries_by_cases):

    if country[0] == "Argentina":

        # argentina line cases
        plt.plot(dates[country], total_cases[country],
                argentina_marker, label=country[0], color=color_map[i],
                alpha=argentina_alpha, linewidth=argentina_linewidth,
                markeredgecolor=argentina_markeredgecolor,
                markersize=argentina_markersize,
                zorder=10)

        # argentina recovered region
        total_recovered_death = [a + b for a,b in zip(total_recovered[country],total_deaths[country])]
        ax.fill_between(dates[country], total_cases[country], total_recovered_death,
                        color=color_map[i], alpha=0.05, zorder=1)
        plt.plot(dates[country], total_recovered[country],
                '--', color=color_map[i], alpha=0.4, linewidth=1,
                zorder=1)

        # argentina markers
        for j, date in enumerate(dates[country]):
            if j == 0 or date < all_dates[0]:
                continue

            # new cases
            new_cases = str(total_cases[country][j] - total_cases[country][j-1])

            # we have new deaths
            if total_deaths[country][j] != total_deaths[country][j - 1]:

                # new deaths
                new_deaths = str(total_deaths[country][j] - total_deaths[country][j-1])

                # big death marker 
                plt.plot(dates[country][j], total_cases[country][j],
                    argentina_marker, label=country[0], 
                    color=color_death,
                    alpha=argentina_alpha, linewidth=argentina_linewidth,
                    markeredgecolor=argentina_markeredgecolor,
                    markersize=18,
                    zorder=11)
                
                # new death number inside marker
                ax.text(dates[country][j], total_cases[country][j],
                    "+" + new_deaths, 
                    fontsize=8, fontfamily='serif',
                    color='white',
                    va='center', ha='center',
                    zorder=12)

                # new cases number outside marker
                trans_offset = offset_copy(ax.transData, x=0, y=0.12, units='inches', fig=fig)
                ax.text(date, total_cases[country][j],
                        "+" + new_cases, 
                        transform=trans_offset,
                        fontsize=8, fontfamily='serif',
                        color='dimgray',
                        va='bottom', ha='right', rotation=-15)

                # new tests below marker
                spaces = "      "
                if j < len(total_tests[country]) and total_tests[country][j] - total_tests[country][j-1] != 0:
                    if total_tests[country][j] > 0:
                        ax.text(date, total_cases[country][j],
                                "+" + str(total_tests[country][j] - total_tests[country][j-1]) + spaces, 
                                fontsize=8, fontfamily='serif',
                                color=test_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=2)

            # we have no new deaths
            else:
                # new cases number outside marker
                trans_offset = offset_copy(ax.transData, x=0, y=0.06, units='inches', fig=fig)
                ax.text(date, total_cases[country][j],
                        "+" + new_cases, 
                        transform=trans_offset,
                        fontsize=8, fontfamily='serif',
                        color='dimgray',
                        va='bottom', ha='right', rotation=-15)
                
                # new tests below marker
                spaces = "    "
                if j < len(total_tests[country]) and total_tests[country][j] - total_tests[country][j-1] != 0:
                    if total_tests[country][j] > 0:
                        ax.text(date, total_cases[country][j],
                                "+" + str(total_tests[country][j] - total_tests[country][j-1]) + spaces, 
                                fontsize=8, fontfamily='serif',
                                color=test_color,
                                va='top', ha='center',
                                rotation=90,
                                zorder=2,)
            
            # death ratio circle on the back
            plt.plot(dates[country][j], total_cases[country][j],
                    argentina_marker, label=country[0], 
                    color=color_rate,
                    alpha=0.2,
                    markeredgecolor=argentina_markeredgecolor,
                    markersize= 20 * 100 * death_ratio[country][j],
                    zorder=1)

    # other country (provinces)
    else:
        # line
        plt.plot(dates[country], total_cases[country],
                country_marker, label=country[0], color=color_map[i],
                alpha=country_alpha, linewidth=country_linewidth,
                markeredgecolor='black', markersize=country_markersize)

        # first and last points
        plt.plot(dates[country][-1], total_cases[country][-1],
                'o', label=country[0], color=color_map[i], 
                alpha=1, linewidth=country_linewidth,
                markersize=country_markersize,  markeredgecolor='black')
        plt.plot(dates[country][0], total_cases[country][0],
                'o', label=country[0], color=color_map[i], 
                alpha=1, linewidth=country_linewidth, 
                markersize=country_markersize, markeredgecolor='black')

        # prvinces death markers
        for j, date in enumerate(dates[country]):
            if j == 0 or date < all_dates[0]:
                continue
            
            # we have new deaths
            if total_deaths[country][j] != total_deaths[country][j - 1]:

                # new deaths
                new_deaths = str(total_deaths[country][j] - total_deaths[country][j-1])

                # offset if overlap
                y_offset_data = 0
                for other_country in countries_by_cases:
                    if other_country == country:
                        continue

                    if date in dates[other_country]:
                        index = dates[other_country].index(date)
                        # also death here today
                        if total_deaths[other_country][index] != total_deaths[other_country][index - 1]:
                            # check if overlapped
                            x, y_1 = ax.transData.transform([mdates.date2num(date),total_cases[country][j]])
                            x, y_2 = ax.transData.transform([mdates.date2num(date),total_cases[other_country][index]])                
                            y_diff_pixels = y_1 - y_2
                            y_diff_points = y_diff_pixels * 72./fig.dpi

                            if y_diff_points > 0 and y_diff_points < 12 or (y_diff_points == 0 and country > other_country):
                                # compute postive data_offset
                                y_offset_points = (12 - y_diff_points) / 2
                                y_offset_pixels = y_offset_points / (72./fig.dpi)
                                x, y_new_data = ax.transData.inverted().transform([0, y_1 + y_offset_pixels])
                                y_offset_data = y_new_data - total_cases[country][j]

                            if y_diff_points > -12 and y_diff_points < 0 or (y_diff_points == 0 and country > other_country):
                                # compute negative data_offset
                                y_offset_points = - (12 + y_diff_points) / 2
                                y_offset_pixels = y_offset_points / (72./fig.dpi)
                                x, y_new_data = ax.transData.inverted().transform([0, y_1 + y_offset_pixels])
                                y_offset_data = y_new_data - total_cases[country][j]

                # big death marker 
                plt.plot(dates[country][j], total_cases[country][j] + y_offset_data,
                    argentina_marker, label=country[0], 
                    color=color_map[i], transform=ax.transData,
                    alpha=1, linewidth=country_linewidth,
                    markeredgecolor='black',
                    markersize=12,
                    zorder=11)

                # new death number inside marker
                color = 'white'
                if j < 15:
                    color = 'black'
                ax.text(dates[country][j], total_cases[country][j] + y_offset_data,
                    "+" + new_deaths, transform=ax.transData,
                    fontsize=5, fontfamily='serif',
                    color=color,
                    va='center', ha='center',
                    zorder=12)

    # text column on the right
    start = 0 # bottom
    end = 0.85   # up    

    if country[0] == "Argentina":

        text = "Argentina, "+ dates[country][-1].strftime('%d/%m') + ":"
        text += "\n(" + str(total_cases[country][-1]) + ") casos"
        text += "\n(" + str(total_deaths[country][-1]) + ") fallecidos"
        text += "\n(" + str(round(100 * death_ratio[country][-1], 1)) + "%) tasa let."
        text += "\n(" + str(total_tests[country][-1]) + ") tests"
        text += "\n(" + str(total_recovered[country][-1]) + ") recuperados"
        text += "\n(" + str(total_cases[country][-1] - total_recovered[country][-1] - total_deaths[country][-1]) + ") activos"

        ax.annotate(text,
                    xy=(dates[country][-1], total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5), total_cases[country][-1]),
                    # textcoords=trans,
                    bbox=dict(boxstyle="round",
                        color=color_back,
                        alpha=0.5,
                        ec="none"),
                    ha="left", va="center", fontsize=10)
    else:

        text = "(" + str(total_cases[country][-1]) + ") " + country[0]
        ax.text(dates[country][-1] + timedelta(days=1.5),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text,
                transform=trans,
                ha="left", va="center", fontsize=10)

        text_d = "(" + str(total_deaths[country][-1]) + ")"
        ax.text(dates[country][-1] + timedelta(days=6.9),
                start + (i + 0.5) * (end-start) / len(countries_by_cases),
                text_d,
                transform=trans,
                ha="right", va="center", fontsize=10,
                bbox=dict(boxstyle="round",
                        # fc=(0.7, 0.7, 1.0),
                        color=color_back,
                        alpha=0.5,
                        ec="none"),)
    
    # arrow
    if country[0] == "Argentina":
            ax.annotate("",
                    xy=(dates[country][-1], total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5), total_cases[country][-1]),
                    # textcoords=trans,
                    arrowprops=dict(arrowstyle="-", color=color_map[i],
                                    connectionstyle="arc,rad=-0.1",
                                    ls='-', lw=1))
    else:
        ax.annotate("",
                    xy=(dates[country][-1], total_cases[country][-1]),
                    xytext=(dates[country][-1] + timedelta(days=1.5),
                            start + (i + 0.5) * (end-start) / len(countries_by_cases)),
                    textcoords=trans,
                    arrowprops=dict(arrowstyle="-", color=color_map[i],
                                    connectionstyle="arc,rad=-0.1",
                                    ls='-', lw=1))

# label (uses same i)
text = "(" + "casos" + ") " + "provincia"
ax.text(dates[country][-1] + timedelta(days=1.5),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.008,
        text, color='gray',
        transform=trans,
        ha="left", va="center", fontsize=8)
text_d = "(" + "fall." + ")"
ax.text(dates[country][-1] + timedelta(days=6.9),
        start + (i + 0.5) * (end-start) / len(countries_by_cases) - 0.008,
        text_d, color='gray',
        transform=trans,
        ha="right", va="center", fontsize=8)

plt.title("Casos confirmados de COVID-19 en Argentina")

# linea de eventos
events = [
    ("2020-03-30", "Ampliación def.\nde caso (local)"),
    ("2020-03-22", "Ampliación def.\nde caso (directo)"),
    ("2020-03-20", "Cuarentena\ntotal y\nobligatoria"),
    ("2020-03-16", "Cierre de\nfronteras"),
    ("2020-03-15", "Suspensión\nfútbol y\nclases"),
    ("2020-03-12", "Suspensión\nespectáculos"),
    ("2020-03-11", "Cuarentena\na viajeros"),
]

for i, event in enumerate(events):
    date_time_line = datetime.datetime.strptime(event[0], '%Y-%m-%d')
    if date_time_line < all_dates[0]:
        continue
    x_axis = 0.3
    ha = 'right'
    if date_time_line < all_dates[4]:
        ha = 'left'
        x_axis = 0.1
    y_axis = 0.97 - i * 0.055
    ax.axvline(date_time_line, color='gray', lw=1, ls=':')
    # highlight cuarentena start
    if event[0]=="2020-03-20":
        ax.axvline(date_time_line - timedelta(days=0.1), color='gray', lw=0.5, ls='-')
    textstr = event[1]
    ax.text(x_axis, y_axis, textstr,
            transform=ax.transAxes, 
            fontsize=8, fontfamily='serif',
            va='center', ha=ha)
            #va='center', ha='right') # hist
    ax.annotate("",
                xy=(mdates.date2num(date_time_line), y_axis), xycoords=trans,
                xytext=(x_axis, y_axis), textcoords=ax.transAxes, 
                arrowprops=dict(arrowstyle="<-", color='gray', ls='-', lw=1))

# leyenda
x_axis = 0.17 
y_axis = 0.195
bb = transforms.Bbox([[x_axis-0.14, y_axis-0.175], [x_axis+0.14, y_axis+0.045]]) 
p_bbox = FancyBboxPatch((bb.xmin, bb.ymin),
            abs(bb.width), abs(bb.height),
            transform=ax.transAxes, 
            boxstyle="round,pad=0",
            # fc='lightgray',
            fc='white',
            ec='gray',
            alpha=0.9,
            zorder=20)
ax.add_patch(p_bbox)

plt.plot(x_axis, y_axis,
    argentina_marker, 
    transform=ax.transAxes, 
    label=country[0], 
    color=color_death,
    alpha=argentina_alpha, linewidth=argentina_linewidth,
    markeredgecolor=argentina_markeredgecolor,
    markersize=18,
    zorder=22)

plt.plot(x_axis, y_axis,
    argentina_marker,
    label=country[0], 
    transform=ax.transAxes, 
    color=color_rate,
    alpha=0.2,
    markeredgecolor=argentina_markeredgecolor,
    markersize= 20 * 1.7,
    zorder=22)

spaces = "     " # tests
ax.text(x_axis, y_axis,
        "T" + spaces, 
        transform=ax.transAxes, 
        fontsize=8, fontfamily='serif',
        color=test_color,
        va='top', ha='center',
        rotation=90,
        bbox=dict(boxstyle="round,pad=0.2",
            alpha=0.5,
            ec=(0.9, 0.9, 0.9),
            fc=(0.9, 0.9, 0.9),),
        zorder=21)

ax.text(x_axis, y_axis, "+F", 
    transform=ax.transAxes, 
    fontsize=8, fontfamily='serif',
    color='white',
    va='center', ha='center',
    zorder=23)

ax.text(x_axis - 0.02, y_axis + 0.01, "+C", 
    transform=ax.transAxes, 
    fontsize=8, fontfamily='serif',
    color='dimgray',
    va='bottom', ha='right',
    zorder=23, rotation=-15)

text = "+C: nuevos casos\n+F: nuevos fallecidos\n"
text += "+T: tests diarios\nCirc. externo: tasa let.\n"
text += "Línea punt.: recuperados\n"
text += "Área rosa: activos (2)\n"
ax.text(x_axis, y_axis - 0.12, text,
        transform=ax.transAxes, 
        fontsize=8, fontfamily='serif',
        va='center', ha='center',
        zorder=23)

# white rect
date_time_line = datetime.datetime.strptime("2020-03-20", '%Y-%m-%d')
left, bottom = mdates.date2num(date_time_line), 1
width, height = mdates.date2num(all_dates[-1]) - mdates.date2num(date_time_line), plt.ylim()[1]
ax.add_patch(plt.Rectangle((left, bottom), width, height,
            facecolor="lavender", alpha=0.1, zorder=-10))

# text fuentes
text1 = 'Fuentes: https://www.argentina.gob.ar/coronavirus/informe-diario, https://www.argentina.gob.ar/coronavirus/medidas-gobierno\n'
text1 += 'Datos: https://github.com/SistemasMapache/Covid19arData (@infomapache), tests cargados por @jorgealiaga.\n'
text1 += '(1) Casos acumulados (recuperados, activos, y fallecidos). (2) Activos = casos - recuperados - fallecidos'
text2 = 'Imagen:\njuanfraire@gmail.com'
plt.text(0.1, 0.02, text1, fontsize=6,
         transform=plt.gcf().transFigure, ha="left", va="center")
plt.text(0.93, 0.02, text2, fontsize=6,
         transform=plt.gcf().transFigure, ha="right", va="center")


plt.savefig("figure-arg-cases.png", format='png')
plt.show()